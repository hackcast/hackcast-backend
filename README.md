# HackCast Backend

Backend for Hackcast app

## Plan

- Allow user to upload 'zip' file
- Ingest this file, process and store in database somehow

## Running

go build && ./hackcast-backend

# JSON spec

Hackcast files are formed as a zip of nested json along with media files.

## META.json

All valid Hackcast files contain a 'META.json' in their root directory. Example:

```json
{
    "slug": "hi134",
    "title": "HI #134: Boxing Day",
    "desc": "On a chill boxing day, Grey and Brady discuss: post-final-Star-wars-trilogy feelings, the endings of things, Cybertruck, xmas hotstoppers, gong baths, imagine an apple revisited, Brady's idea for youtube rewind, still more straws, and meditation... results?",
    "entry": "hi134.json"
}
```

This is relatively self explanatory, perhaps except for the "entry" field, which simply describes the entry point (another json file) which is a piece of media.

## Media json

All other json files describe media. Actual media files (either remote or inside the folder somewhere) are referenced by these json documents.

Example:

```json
{
	"type": "audio",
    "location": "local",
	"resource": "HI134.mp3"
}
```

Type: describes the media type. For now: audio, image, or markdown.
Location (local | remote): whether "resource" points to a path inside the folder or a remote uri
Resource: either a path to a local file within the zip folder or a web-accessible uri

### Audio json

Audio media json can have two special fields:

```json
{
    "type": "audio",
    "location": "local",
    "resource": "HI134.mp3",
    "contents": {
        "0": "Intro",
        "100": "Star Wars Discussion",
        "1532": "Tesla Cybertruck Discussion",
        "2000": "Ad break",
        "2120": "Christmas Village Nightmare"
    },
    "triggers": {
        "0": {
            "type": "displaytext",
            "text": "hello"
        }
    }
}
```

Contents is a json object of strings to strings, which describes the table of contents for this audio resource.

Triggers define when interactivity will happen. The "triggers" object maps timestamps (seconds, in string form) to event objects, which contain various data on triggers.

In theory, on the frontend triggers basically act as the "state" of the podcast, and users move between states either by interacting with the current triggered event or by waiting for another event to be triggered by the timestamp changing.

| event type       | parameters          | description                                                                                                     |
|------------------|---------------------|-----------------------------------------------------------------------------------------------------------------|
| clear_canvas     |                     | clears the canvas view on user's phone                                                                          |
| display_image    | resource            | displays an image in the canvas view                                                                            |
| display_text     | text                | displays text in the canvas view                                                                                |
| display_markdown | md || resource      | displays markdown -- either takes in inline markdown within the file or a reference to a markdown resource js   |
| display_link     | text, url           | displays link as a button - when clicked on, opens up a webview in the app                                      |
| display_button   | text, event         | displays a button with text - when clicked on, triggers another event                                           |
| jump             | timestamp           | jumps to timestamp within this resource                                                                         |
| jump_to_resource | timestamp, resource | jumps to another resource at timestamp                                                                          |
| jump_to_episode  | episode             | jumps to another episode, in format "podcastslug/episodeslug" -- can be an episode of this podcast or any other |
|                  |                     |                                                                                                                 |

# SQL

## Useful stuff

### List all tables

```sql
SELECT
   *
FROM
   pg_catalog.pg_tables
WHERE
   schemaname != 'pg_catalog'
AND schemaname != 'information_schema';
```

## Schemas

### podcasts table

```sql
CREATE TABLE podcasts (
	slug varchar(255) NOT NULL, -- unique id to be used in url path & api calls
	title text,					-- display title
	creator varchar(255),		-- owner of the podcast
	imageurl varchar(255),		-- podcast's "album image", square, probably a link to something in block storage for us
	description text,			-- longform podcast description
	PRIMARY KEY (slug)
)
```

### episodes table

**in progress** but should definitely exist

```sql
CREATE TABLE episodes (
	id SERIAL,
	title varchar(255),
	description text,	
	podcast varchar(255),		
	slug varchar(255),			
	entryname text, --entry point xid for initial audio file
    entrylength int --length in ms of the file
)
```

### resources table

Defines a resource of any sort -- mostly audio, but also potentially text, markdown, images, etc...
In theory, meant to be modular but idk lol 

```sql
CREATE TABLE resources (
	name varchar(255), -- in form podcast/episode/_____.json (could be more directories in the way) -- but keep the original filesystem format cause why not???
	type varchar(255),
	resourceuri text,
	contents json,
	triggers json,
	PRIMARY KEY (name)
)
```



### user table

**in progress** but should definitely exist

## Other details

- Recommended you install pgadmin4 to debug database easily