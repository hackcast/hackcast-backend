//Functions to manage podcasts and episodes

package main

import (
	"container/list"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

type Podcast struct {
	slug        string
	title       string
	owner       string
	imageurl    string
	description string
}

type PodcastOutput struct {
	Slug        string `json:"slug"`
	Title       string `json:"title"`
	Owner       string `json:"owner"`
	Imageurl    string `json:"imageurl"`
	Description string `json:"description"`
}

type Episode struct {
	id          int
	title       string
	description string
	podcast     string
	slug        string
	entryname   string
	entrylength int
}

type Meta struct {
	Slug  string `json:"slug"`
	Title string `json:"title"`
	Desc  string `json:"desc"`  //unique slug of podcast -- TODO: consider whether we're going to allow people to change podcast slugs... idk if it's worth designing around this consideration
	Entry string `json:"entry"` //starter file
}

type Resource struct {
	Type     string            `json:"type"`
	Location string            `json:"location"`
	Resource string            `json:"resource"`
	Contents map[string]string `json:"contents"`
	Triggers interface{}       `json:"triggers"`
}

//Adds a row to the 'podcasts' table
//Requires there to be a global db variable exposing a SQL connection
func CreatePodcast(slug string, title string, owner string, imageurl string, description string) error {
	sqlTemplate := `
	INSERT INTO podcasts (slug, title, creator, imageurl, description)
	VALUES ('%s', '%s', '%s', '%s', '%s')
	`

	sqlStatement := fmt.Sprintf(sqlTemplate, slug, title, owner, imageurl, description)

	_, err := db.Exec(sqlStatement)

	return err
}

//Gets a row from the 'podcasts' table
//Requires there to be a global db variable exposing a SQL connection
//Returns a podcast struct
func GetPodcast(slug string) (*Podcast, error) {
	sqlTemplate := `SELECT title, creator, imageurl, description FROM podcasts WHERE slug='%s';`
	sqlStatement := fmt.Sprintf(sqlTemplate, slug)

	var title string
	var creator string
	var imageurl string
	var description string

	row := db.QueryRow(sqlStatement)
	err := row.Scan(&title, &creator, &imageurl, &description)

	return &Podcast{slug, title, creator, imageurl, description}, err
}

func GetPodcasts(owner string) ([]PodcastOutput, error) {
	sqlTemplate := `SELECT title, slug, imageurl, description FROM podcasts WHERE creator=$1;`

	rows, err := db.Query(sqlTemplate, owner)
	defer rows.Close()

	fmt.Println("still good")
	out := make([]PodcastOutput, 0)
	fmt.Println(rows)

	if rows == nil {
		return nil, fmt.Errorf("no podcasts")
	}

	for rows.Next() {

		var title string
		var slug string
		var imageurl string
		var description string

		err = rows.Scan(&title, &slug, &imageurl, &description)
		if err != nil {
			return nil, err
		}

		fmt.Println("hello")
		out = append(out, PodcastOutput{slug, title, owner, imageurl, description})
		fmt.Println("goodbye")
	}

	return out, nil
}

//Updates a row in the 'resources' table -- misnomer? I think so too. But we're in too deep now, buddy.
func UpdatePodcast(name string, contents interface{}, triggers interface{}) error {
	sqlStatement := `UPDATE resources SET contents=$2, triggers=$3 WHERE name = $1;`

	onestring, _ := json.Marshal(contents)
	twostring, _ := json.Marshal(triggers)

	_, err := db.Exec(sqlStatement, name, string(onestring), string(twostring))
	return err
}

//Adds a row to the 'episode' table
//Requires there to be a global db variable exposing a SQL connection
func CreateEpisode(podcast string, entryname string, entrylength int, meta Meta) error {
	_, err := GetEpisode(meta.Slug, podcast)
	if err == nil {
		return fmt.Errorf("A podcast with that slug exists")
	}

	sqlTemplate := `
	INSERT INTO episodes (title, description, podcast, slug, entryname, entrylength)
	VALUES ($1, $2, $3, $4, $5, $6)
	`

	//sqlStatement := fmt.Sprintf(sqlTemplate, meta.Title, meta.Desc, podcast, meta.Slug, entryname, entrylength)

	_, err = db.Exec(sqlTemplate, meta.Title, meta.Desc, podcast, meta.Slug, entryname, entrylength)

	return err
}

//TODO: delete episode
func DeleteEpisode(podcast string) {

}

//Adds a row to the 'resources' table
//Resources will be mostly audio files, but can also represent other things like text, images, etc.
//contents and triggers are both json strings, and are optional. They will generally only be used when the resource is audio
func CreateResource(name string, rType string, resourceuri string, contents string, triggers string) error {
	sqlTemplate := `
	INSERT INTO resources (name, type, resourceuri, contents, triggers)
	VALUES ($1, $2, $3, $4, $5)
	`

	_, err := db.Exec(sqlTemplate, name, rType, resourceuri, contents, triggers)

	return err
}

//Gets a row from the 'episode' table
//Requires there to be a global db variable exposing a SQL connection
//Returns a episode struct
func GetEpisode(slug string, podcastslug string) (*Episode, error) {
	sqlTemplate := `SELECT id, title, description, podcast, slug, entryname, entrylength FROM episodes WHERE slug='%s' AND podcast='%s';`
	sqlStatement := fmt.Sprintf(sqlTemplate, slug, podcastslug)

	var id int
	var title string
	var description string
	var podcast string
	var pslug string
	var entryname string
	var entrylength int

	row := db.QueryRow(sqlStatement)
	err := row.Scan(&id, &title, &description, &podcast, &pslug, &entryname, &entrylength)

	return &Episode{id, title, description, podcast, pslug, entryname, entrylength}, err
}

func GetEpisodes(podcastslug string) ([]Episode, error) {
	sqlTemplate := `SELECT id, title, description, podcast, slug, entryname, entrylength FROM episodes WHERE podcast=$1 ORDER BY id DESC;`

	rows, err := db.Query(sqlTemplate, podcastslug)
	defer rows.Close()

	out := make([]Episode, 0)

	for rows.Next() {
		var id int
		var title string
		var description string
		var podcast string
		var pslug string
		var entryname string
		var entrylength int

		err = rows.Scan(&id, &title, &description, &podcast, &pslug, &entryname, &entrylength)
		if err != nil {
			return nil, err
		}

		out = append(out, Episode{id, title, description, podcast, pslug, entryname, entrylength})
	}

	return out, nil
}

//Gets a row from the 'resource' table
//Requires there to be a global db variable exposing a SQL connection
func GetResource(podcastSlug string, episodeSlug string, resourceName string) map[string]string {
	queryName := fmt.Sprintf(`'%s'`, resourceName)
	sqlStatement := `SELECT name, type, resourceuri, contents, triggers FROM resources WHERE name=` + queryName

	var name string
	var rType string
	var resourceuri string
	var contents string
	var triggers string

	row := db.QueryRow(sqlStatement)
	err := row.Scan(&name, &rType, &resourceuri, &contents, &triggers) //TODO: error handling
	checkErr(err)

	m := make(map[string]string)

	m["name"] = name
	m["type"] = rType
	m["resource"] = resourceuri
	m["contents"] = contents
	m["triggers"] = triggers

	fmt.Println(m)

	return m
}

//Reterns the uri of a resource
func GetResourceUri(resourceName string) (string, error) {
	sqlStatement := `SELECT resourceuri FROM resources WHERE name=$1`

	var resourceuri string

	row := db.QueryRow(sqlStatement, resourceName)
	err := row.Scan(&resourceuri)

	return resourceuri, err
}

//Unzips a podcast file
//	Uploads relevant files to block storage
//	Stores files in database using unique identifiers
//	Parses JSON, update it using new routes, then store as well
//	See: https://github.com/gin-gonic/examples/blob/master/upload-file/single/main.go
func ProcessZip(podcastslug string, c *gin.Context) {

	file, err := c.FormFile("file") //gets file from form
	if sendErr(err, c) {
		return
	}

	filename := filepath.Base(file.Filename)
	dir, err := ioutil.TempDir("", "hackcast")
	if sendErr(err, c) {
		return
	}

	//We later delete the entire directory that will later contain our episode
	//So: ENSURE that all files are PROCESSED AND UPLOADED TO BLOCK STORAGE before doing this
	//TODO: uncomment this line defer os.RemoveAll(dir)

	zippath := filepath.Join(dir, filename)

	fmt.Println(zippath)

	err = c.SaveUploadedFile(file, zippath)
	if sendErr(err, c) {
		return
	}

	//At this point, zip file should now be at zippath

	fileNames, err := Unzip(zippath, dir)
	checkErr(err)

	fmt.Println("unzip successs woo")

	//Files should now be at dir
	dir = path.Clean(fileNames[0]) //output dir

	//Process 'META.json' in root directory -- is a requirement so throw an error if can't be found
	jsonFile, err := os.Open(filepath.Join(dir, "META.json"))
	if sendErr(err, c) {
		return
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var metadata Meta

	json.Unmarshal(byteValue, &metadata)

	fmt.Println(metadata)

	/**

	WHY IS THE FILE BEING UPLOADED HERE? ISNT ALL RELEVANT METADATA GOING TO BE STORED ANYWAY??
	Commenting out for the time being

	name, err := UploadFile(dir, "META.json", podcastslug, c)
	if sendErr(err, c) {
		return
	}
	**/

	blockPrefix := podcastslug + "/" + metadata.Slug // [podcast name]/[episode name] -- makes sense?

	err = CreateEpisode(podcastslug, blockPrefix+"/"+metadata.Entry, 0, metadata) //TODO: figure out how to calculate entry length
	checkErr(err)

	//now fuck with the JSON: https://tutorialedge.net/golang/parsing-json-with-golang/
	queue := list.New() //"absolute paths" WITHIN dir (ie excluding dir)
	queue.PushBack(metadata.Entry)

	seen := make(map[string]bool)

	for queue.Len() > 0 { //iterate through queue
		elem := queue.Front()
		epath := elem.Value.(string) //get value and assert type
		queue.Remove(elem)

		if seen[epath] {
			continue
		}

		seen[epath] = true

		folder := path.Dir(epath) //to construct new paths :) :) :) :)
		absolute := path.Join(dir, epath)

		jsonFile, err := os.Open(absolute)
		fmt.Println("opened:" + absolute)
		if sendErr(err, c) {
			fmt.Println("oof error: couldn't open file. Probably a bad reference in the json")
			return
		}

		byteValue, _ := ioutil.ReadAll(jsonFile)
		var resource Resource
		json.Unmarshal(byteValue, &resource)

		if resource.Location == "local" { //references a local file
			resourceLocalPath := path.Join(folder, resource.Resource)
			fileName, err := UploadFile(dir, resourceLocalPath, blockPrefix, c)
			checkErr(err) //sendErr(err, c)
			resource.Resource = ObjectUrl(fileName)
		} //resouce.Resource should now point to a legit uri

		jstring, err := json.Marshal(resource.Contents)
		checkErr(err)

		//TODO: run through triggers and update resources -- OH WAIT!! now we don't need to update the resource names cause they're all associated w/ json... just need to traverse the path and actually upload resources

		trigs := ProcessResources(folder, blockPrefix, resource.Triggers, queue)

		trigString, err := json.Marshal(trigs)
		fmt.Printf("%+v", resource.Triggers)
		checkErr(err)

		err = CreateResource(path.Join(blockPrefix, epath), resource.Type, resource.Resource, string(jstring), string(trigString))
		sendErr(err, c)

		//TODO: traverse the other resources and stuff
	}

}
