module gitlab.com/hackcast/hackcast-backend

go 1.13
// +heroku goVersion go1.13

require (
	cloud.google.com/go/storage v1.5.0
	github.com/auth0-community/go-auth0 v1.0.0
	github.com/auth0/go-jwt-middleware v0.0.0-20190805220309-36081240882b
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.5.0
	github.com/go-ini/ini v1.51.1 // indirect
	github.com/lib/pq v1.3.0
	github.com/minio/minio-go v6.0.14+incompatible
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/rs/xid v1.2.1
	github.com/tidwall/gjson v1.3.5 // indirect
	golang.org/x/crypto v0.0.0-20200115085410-6d4e4cb37c7d // indirect
	golang.org/x/sys v0.0.0-20200107162124-548cf772de50 // indirect
	google.golang.org/api v0.15.0
	gopkg.in/square/go-jose.v2 v2.4.1
)
