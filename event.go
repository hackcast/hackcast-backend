package main

import (
	"container/list"
	"fmt"
	"path"
)

type Event struct {
	Type string
}

const hosturl = "http://138.68.25.217:8080/resource?name="

//Runs through all of the triggers, finds resources and adds them to the queue
//Also updates triggers to use 'absolute' paths -- uses folder and paths.join for this
func ProcessResources(folder string, blockprefix string, triggers interface{}, queue *list.List) map[string]map[string]string {
	if triggers == nil {
		return nil
	}

	fmt.Println(triggers)

	trigs := triggers.(map[string]interface{})

	ts := make(map[string]map[string]string)

	for k, _ := range trigs {
		n := trigs[k].(map[string]interface{})
		sub := make(map[string]string)
		for key, val := range n {
			sub[key] = fmt.Sprintf("%v", val)
		}
		ts[k] = sub
	}

	//processes triggers -- just works for non-nested

	for k, _ := range ts {
		if val, ok := ts[k]["resource"]; ok {
			p := path.Join(folder, val)
			queue.PushBack(p)
			ts[k]["resource"] = hosturl + blockprefix + "/" + p
		}
	}

	//TODO: make it work for NESTED... right now we'll just make it work for single

	/**
	for k, _ := range triggers {
		ok := true
		var curr map[string]interface{}
		curr = triggers[k]
		for ok {
			if val, ok := curr["resource"]; ok {
				queue.PushBack(paths.Join(folder, val))
				curr["resource"] = blockprefix + "/" + paths.Join(folder, val)
				curr = curr["event"]
			}
		}
	}

	**/

	fmt.Println(ts)
	return ts
}
