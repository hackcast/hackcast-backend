package main

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	minio "github.com/minio/minio-go"
)

//InitDB initializes SQL connection and returns a pointer to the open db connection
//This pointer can then be stored in a global variable for easy access
func InitDB(db_host string, db_user string, db_pass string, db_name string, db_port string) *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=require", db_host, db_user, db_pass, db_name, db_port)
	var err error
	database, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	err = database.Ping()
	if err != nil {
		panic(err)
	}

	return database
}

//InitStorage initializes an S3 client and returns a pointer to this client
//This pointer can then be stored in a global variable for easy access
func InitStorage(endpoint string, key string, secret string) *minio.Client {

	client, err := minio.New(endpoint, key, secret, true) //true == enable ssl
	checkErr(err)

	spaces, err := client.ListBuckets()
	checkErr(err)

	for _, space := range spaces {
		fmt.Println(space.Name)
	}

	return client
}

//UploadFile ingests a file (given a dir and path within it), names it with the prefix+path and then uploads it to block storage
//Returns the string id for the block storage
func UploadFile(dir string, path string, prefix string, c *gin.Context) (string, error) {
	name := fmt.Sprintf("%s/%s", prefix, path)

	fpath, _ := filepath.Abs(filepath.Join(dir, path))

	file, err := os.Open(fpath)
	if sendErr(err, c) {
		return "", err
	}

	contentType, err := GetFileContentType(file)
	if sendErr(err, c) {
		return "", err
	}

	userMetaData := map[string]string{"x-amz-acl": "public-read"} //so file is public
	_, err = s3.FPutObject("hackcast", name, fpath, minio.PutObjectOptions{ContentType: contentType, UserMetadata: userMetaData})
	if sendErr(err, c) {
		return "", err
	}

	return name, nil
}

//Utility for getting the url for a given S3 object
func ObjectUrl(name string) string {
	return fmt.Sprintf("https://hackcast.sfo2.digitaloceanspaces.com/%s", name)
}

//Debugging tool, prints a current database connection
func PrintDB(database *sql.DB) {
	fmt.Println("------- DATABASE CONNECTION -------")
	fmt.Printf("\n %+v \n \n", database)
	fmt.Println("------- END DATABASE CONNECTION -------")
}
