package main

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	minio "github.com/minio/minio-go"
)

const bucketUrl = "https://hackcast.sfo2.digitaloceanspaces.com"

var db *sql.DB //podcast feeds table
var s3 *minio.Client

func main() {

	var port = 8080

	if os.Getenv("PORT") != "" {
		var err error
		port, err = strconv.Atoi(os.Getenv("PORT"))
		checkErr(err)
	}

	//open database connection and store in global variable
	creds := PostgresCreds()
	db = InitDB(creds["POSTGRES_HOST"], creds["POSTGRES_USER"], creds["POSTGRES_PASS"], creds["POSTGRES_DB"], creds["POSTGRES_PORT"])
	storageCreds := S3Creds()
	s3 = InitStorage(storageCreds["S3_ENDPOINT"], storageCreds["S3_KEY"], storageCreds["S3_SECRET"])

	//debugging - print db connection
	PrintDB(db)

	r := gin.Default()
	r.Use(CORSMiddleware())
	initializeRoutes(r)

	fmt.Printf("Serving app on port %d \n", port)
	r.Run(fmt.Sprintf(":%d", port)) // listen and serve on 0.0.0.0:8080
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
