package main

import (
	"archive/zip"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
)

//Sends a 400 error message to the client
func send400(msg string, c *gin.Context) {
	c.JSON(400, gin.H{
		"message": msg,
	})
}

func send404(c *gin.Context) {
	c.JSON(400, gin.H{
		"message": "resource not found",
	})
}

//checkErr checks for an error, and terminates the program if it exists.
func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

//sendErr checks for an error, and sends back a 500 error to the context
func sendErr(err error, c *gin.Context) bool {
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("got err: %s", err.Error()))
		return true
	}
	return false
}

//Unzips a zip file to destination path
func Unzip(src string, dest string) ([]string, error) {

	fmt.Println("UNZIPPING!!!!!")

	var filenames []string

	r, err := zip.OpenReader(src)
	if err != nil {
		return filenames, err
	}
	defer r.Close()

	for _, f := range r.File {

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dest, f.Name)

		// Check for ZipSlip. More Info: http://bit.ly/2MsjAWE
		if !strings.HasPrefix(fpath, filepath.Clean(dest)+string(os.PathSeparator)) {
			return filenames, fmt.Errorf("%s: illegal file path", fpath)
		}

		filenames = append(filenames, fpath)

		if f.FileInfo().IsDir() {
			// Make Folder
			os.MkdirAll(fpath, os.ModePerm)
			continue
		}

		// Make File
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
			return filenames, err
		}

		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return filenames, err
		}

		rc, err := f.Open()
		if err != nil {
			return filenames, err
		}

		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop
		outFile.Close()
		rc.Close()

		if err != nil {
			return filenames, err
		}
	}

	return filenames, nil
}

//Returns list of all non-json files in dir
func searchDir(dir string) ([]string, error) {
	fileList := []string{}

	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if path[len(path)-5:] != ".json" {
			fileList = append(fileList, path)
		}
		return nil
	})

	return fileList, err
}

func GetFileContentType(in *os.File) (string, error) {

	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)

	_, err := in.Read(buffer)
	if err != nil {
		return "", err
	}

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	return contentType, nil
}

func Value(c map[string]string) (driver.Value, error) {
	return json.Marshal(c)
}
