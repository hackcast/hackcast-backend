package main

import (
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/gin"
)

type EpisodeOutput struct {
	rType       string
	Description string      `json:"description"`
	Entrylength string      `json:"entrylength"`
	Name        string      `json:"name"`
	Resource    string      `json:"resource"`
	Title       string      `json:"title"`
	Triggers    interface{} `json:"triggers"`
	Contents    interface{} `json:"contents"`
}

type SimpleEpisodeOutput struct {
	Id          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Slug        string `json:"slug"`
}

type EpisodeUpdate struct {
	Name     string      `json:"name"`
	Contents interface{} `json:"contents"`
	Triggers interface{} `json:"triggers"`
	Title    string      `json:"title"`
}

// TODO: figure out authentication and make protected routes
// This is actually relatively low priority and will probably be done AFTER Tuesday unless my brain dies and I want some bs to do :)

//Routes are split into multiple functions
//SEE: https://stackoverflow.com/questions/42967235/golang-gin-gonic-split-routes-into-multiple-files for more info

func initializeRoutes(r *gin.Engine) {
	r.GET("/hello", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "hello world!",
		})
	})

	r.GET("/helloerr", func(c *gin.Context) {
		send400("Error: you're not cool enough :)", c)
	})

	r.GET("/resource", func(c *gin.Context) {
		resourceName := c.Query("name")
		uri, err := GetResourceUri(resourceName)
		if err != nil {
			send400("Error: could not find that resource", c)
		} else {
			c.Redirect(302, uri)
		}
	})

	r.GET("/testAuth", authRequired(), func(c *gin.Context) {
		val, _ := c.Get("sub")
		c.JSON(200, gin.H{
			"sub": val,
		})
	})

	//Initialize other routes
	podcastRoutes(r)
	adminRoutes(r)

	//Static home page
	r.StaticFile("/", "./public/index.html")

	//404 route
	r.NoRoute(func(c *gin.Context) {
		send404(c)
	})
}

//Initializes podcast routes
func podcastRoutes(r *gin.Engine) {
	podcasts := r.Group("/podcasts")

	podcasts.GET("/:series/:episode", func(c *gin.Context) {
		podcastSlug := c.Param("series")
		episodeSlug := c.Param("episode")

		ep, err := GetEpisode(episodeSlug, podcastSlug)
		if sendErr(err, c) {
			return
		}

		deets := GetResource(podcastSlug, episodeSlug, ep.entryname)
		deets["title"] = ep.title
		deets["description"] = ep.title
		deets["entrylength"] = string(ep.entrylength)

		one := make(map[string]string)
		var two interface{}

		err = json.Unmarshal([]byte(deets["contents"]), &one)
		err = json.Unmarshal([]byte(deets["triggers"]), &two)

		out := &EpisodeOutput{deets["type"], deets["description"], deets["entrylength"], deets["name"], deets["resource"], deets["title"], two, one}

		c.JSON(200, out)

	})

	podcasts.GET("/:series", func(c *gin.Context) {
		podcastSlug := c.Param("series")

		episodes, err := GetEpisodes(podcastSlug)
		out := make([]*SimpleEpisodeOutput, 0)

		for _, e := range episodes {
			ep := &SimpleEpisodeOutput{e.id, e.title, e.description, e.slug}
			out = append(out, ep)
		}

		if !sendErr(err, c) {
			if len(episodes) > 0 {
				c.JSON(200, out)
			} else {
				c.String(404, "404: your podcast series cannot be found")
			}
		}
	})

	//TODO: make sure that all fields are filled :)
	podcasts.POST("/create", authRequired(), func(c *gin.Context) {
		slug := c.PostForm("slug")
		title := c.PostForm("title")
		owner := c.PostForm("owner")
		imageurl := c.PostForm("imageurl")
		description := c.PostForm("description")

		fmt.Println(owner)
		fmt.Println(slug)
		//for some reason these fields are not escaped/sanitized for SQL
		//should change sql insertion from fmt.Sprintf to db.Exec formatting; see https://www.calhoun.io/what-is-sql-injection-and-how-do-i-avoid-it-in-go/

		err := CreatePodcast(slug, title, owner, imageurl, description)
		if !sendErr(err, c) {
			c.JSON(200, gin.H{
				"message":     "podcast successfully created!",
				"slug":        slug,
				"title":       title,
				"owner":       owner,
				"imageurl":    imageurl,
				"description": description,
			})
		}
	})

	podcasts.POST("/update", func(c *gin.Context) {
		var updateobj EpisodeUpdate
		c.BindJSON(&updateobj)

		fmt.Println(updateobj)

		err := UpdatePodcast(updateobj.Name, updateobj.Contents, updateobj.Triggers)
		if !sendErr(err, c) {
			c.String(200, "success!")
		}
	})

	//A zip should be sent to this file
	//Also takes query param 'podcast' -- the podcast slug we're adding the episode to
	//TODO: require token / auth and validate
	podcasts.POST("/addepisode", func(c *gin.Context) {
		fmt.Println("adding episode....")
		podcast := c.PostForm("podcast")
		if podcast == "" {
			send400("Error: empty podcast string", c)
		}

		_, err := GetPodcast(podcast)
		if err != nil {
			send400("Error: no podcast with that slug found", c)
		}

		ProcessZip(podcast, c) //c contains the uploaded zip file
	})
}

func adminRoutes(r *gin.Engine) {
	admin := r.Group("/admin")

	admin.GET("/mine", authRequired(), func(c *gin.Context) {
		sub, _ := c.Get("sub")
		fmt.Println(sub)
		substring, _ := sub.(string)
		fmt.Println(substring)
		if substring == "" {
			send400("You must be authenticated to make this request", c)
		}
		podcasts, err := GetPodcasts(substring)
		if !sendErr(err, c) {
			fmt.Println(podcasts)
			c.JSON(200, podcasts)
		}
	})
}
